//
//  CurrentCondition.swift
//  Weather-app
//
//  Created by Влад on 4/22/19.
//

import Foundation
import Tailor

struct CurrentConditions: Mappable {
    
    var title: String!
    var icon: Int!
    var epochDate: Double!
    var temperature: Double!
    
    init(_ map: JSONDictionary) {
        title <- map.resolve(keyPath: "WeatherText")
        icon <- map.property("WeatherIcon")
        epochDate <- map.resolve(keyPath: "EpochTime")
        temperature <- map.resolve(keyPath: "Temperature.Metric.Value")
    }
}
