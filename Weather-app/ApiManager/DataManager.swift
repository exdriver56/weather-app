//
//  DataManager.swift
//  Weather-app
//
//  Created by Влад on 4/22/19.
//

import Foundation

class DataManager {
    
    static let shared = DataManager()
    
    fileprivate init () {}
    
    private var locations: [Location] = []
    private var allLocations: [Location] = []
    
    private var forecast: Forecast! = Forecast([:])
    
    private var searchString: String = ""
    
    func searchLocation(searchValue: String) {
        guard !searchValue.isEmpty else {
            self.clearSearchString()
            return
        }
        self.searchString.append(searchValue)
        let result = self.allLocations.filter({$0.city.lowercased().contains(self.searchString.lowercased())})
        self.locations = result
    }
    
    func clearSearchString () {
        self.searchString = ""
        self.locations = allLocations
    }
    
    func getLocations(completion: ((_ locations: [Location]) -> ())?){
        ApiManager.shared.fetchLocations { (error, locations) in
            if let error = error {
                print(error.domain)
                return
            }
            self.locations = locations
            self.allLocations = locations
            if let _ = completion {
                completion?(self.locations)
            }
        }
    }
    
    func getForecast(type: String, range: String, key: String, completion: @escaping (_ forecast: Forecast) -> ()) {
        
        ApiManager.shared.fetchForecast(type: type, range: range, key: key, completion: { (error, forecast) in
            if let error = error {
                print(error.domain)
                return
            }
            
            self.forecast = forecast
            completion(self.forecast)
        })
    }
    
    func getCountOfDailyForecasts () -> Int {
        return self.forecast.dailyForecasts.count
    }
    func getCountOfLocations () -> Int {
        return self.locations.count
    }
    func getLocationByIndex(index: Int) -> Location {
        return self.locations[index]
    }
    func getForecastByIndex(index: Int) -> DailyForecast {
        return self.forecast.dailyForecasts[index]
    }
}
