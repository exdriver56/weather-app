//
//  ApiManager.swift
//  Weather-app
//
//  Created by Влад on 4/11/19.
//

import Foundation
import Alamofire

final class ApiManager {
    
    static let shared = ApiManager ()
    
    fileprivate init () {}
    ///Forecast credentials
    private let forecastBaseUrl = "https://dataservice.accuweather.com"
    private let forecastApiKey = "nFA5uitaXXDakiYMgTQbejSDtT4yIn8P"
    ///Location credentials
    private let locationBaseUrl = "https://dataservice.accuweather.com"
    private let locationApiKey = "nFA5uitaXXDakiYMgTQbejSDtT4yIn8P"
    
    // MARK: - Requests
    
    func fetchCurrentConditions(key: String, completion: @escaping (_ error: NSError?, _ result: [CurrentConditions]?) -> Void) {
        let url = "\(forecastBaseUrl)/currentconditions/v1/\(key)?apikey=\(forecastApiKey)&metric=true"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: [:]).validate().responseJSON{ response in
            
            switch response.result {
            case .success(let JSON):
                
                //Error
                guard let result = JSON as? JSONArray else {
                    completion(NSError(domain: "Failed to fetch current conditions", code: 400, userInfo: nil), nil)
                    return
                }
                
                //Success
                let currentConditions = result.compactMap(CurrentConditions.init)
                
                completion(nil, currentConditions)
                
                break
                
            default:
                completion(NSError(domain: "Failed to fetch current conditions", code: response.response?.statusCode ?? 400, userInfo: nil), nil)
                break
            }
            return
        }
    }
    
    func fetchForecast(type: String, range: String, key: String, completion: @escaping (_ error: NSError?, _ result: Forecast?) -> Void) {
        let url = "\(forecastBaseUrl)/forecasts/v1/\(type)/\(range.lowercased())/\(key)?apikey=\(forecastApiKey)&metric=true"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: [:]).validate().responseJSON{ response in
            
            switch response.result {
            case .success(let JSON):
                
                //Error
                guard let result = JSON as? JSONDictionary else {
                    completion(NSError(domain: "Failed to fetch forecast", code: 400, userInfo: nil), nil)
                    return
                }
                
                //Success
                let forecast = Forecast(result)
                
                completion(nil, forecast)
                
                break
                
            default:
                completion(NSError(domain: "Failed to fetch forecast", code: response.response?.statusCode ?? 400, userInfo: nil), nil)
                break
            }
            return
        }
    }
    
    func fetchLocations(completion: @escaping (_ error: NSError?, _ result: [Location]) -> Void) {
        let url = "\(locationBaseUrl)/locations/v1/topcities/150?apikey=\(locationApiKey)"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: [:]).validate().responseJSON{ response in
            
            switch response.result {
            case .success(let JSON):
                
                //Error
                guard let result = JSON as? JSONArray else {
                    completion(NSError(domain: "Failed to fetch locations", code: 400, userInfo: nil), [])
                    return
                }
                
                //Success
                let locations = result.compactMap(Location.init)
                
                completion(nil, locations)
                
                break
                
            default:
                completion(NSError(domain: "Failed to fetch locations", code: response.response?.statusCode ?? 400, userInfo: nil), [])
                break
            }
            return
        }
    }
}
