//
//  ForecastCollectionViewCell.swift
//  Weather-app
//
//  Created by Влад on 4/22/19.
//

import Foundation
import UIKit

class ForecastCollectionViewCell: UICollectionViewCell {
    
    
    func setupCell (maxTemp: Double, minTemp: Double, dayDescription: String, epochDate: Double, icon: Int) {
        maximumTemperatureLabel.text = "\(Int(maxTemp))°"
        minimumTemperatureLabel.text = "\(Int(minTemp))°"
        imageView.image = UIImage(named: "\(icon)-s")
        descriptionLabel.text = dayDescription.lowercased()
        
        let date = Date(timeIntervalSince1970: epochDate)
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd MMM"
        dateFormatter.string(from: date)
        
        dateLabel.text = dateFormatter.string(from: date).uppercased()
        
        self.layer.cornerRadius = 10
        self.layoutIfNeeded()
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var maximumTemperatureLabel: UILabel!
    @IBOutlet weak var minimumTemperatureLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        containerView.layer.borderColor = UIColor.lightGray.cgColor
    }
}
