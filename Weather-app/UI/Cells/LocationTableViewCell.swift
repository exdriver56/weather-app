//
//  LocationTableViewCell.swift
//  Weather-app
//
//  Created by Влад on 4/22/19.
//

import Foundation
import UIKit

class LocationTableViewCell: UITableViewCell {
    
    func setupCell(city: String, country: String) {
        cityLabel.text = city
        countryLabel.text = country
    }
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
}
