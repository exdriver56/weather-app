//
//  ViewController.swift
//  Weather-app
//
//  Created by Влад on 4/11/19.
//

import UIKit
import CenteredCollectionView

class MainViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    let cellPercentWidth: CGFloat = 0.7
    
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var searchResultViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightOfTableViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTextField: UITextField!
    var centeredCollectionViewFlowLayout: CenteredCollectionViewFlowLayout!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        centeredCollectionViewFlowLayout = collectionView.collectionViewLayout as? CenteredCollectionViewFlowLayout
        
        collectionView.decelerationRate = UIScrollView.DecelerationRate.fast
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        centeredCollectionViewFlowLayout.itemSize = CGSize(
            width: view.bounds.width * cellPercentWidth,
            height: view.bounds.height * cellPercentWidth * cellPercentWidth
        )
        
        centeredCollectionViewFlowLayout.minimumLineSpacing = 20
        
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
}

/// MARK: UICollectionViewDelegate
extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.shared.getCountOfDailyForecasts()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForecastCollectionViewCell", for: indexPath) as! ForecastCollectionViewCell
        let forecast = DataManager.shared.getForecastByIndex(index: indexPath.row)
        cell.setupCell(maxTemp: forecast.maximumTemperature, minTemp: forecast.minimumTemperature, dayDescription: forecast.dayDescription, epochDate: forecast.epochDate, icon: forecast.dayIcon)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentCenteredPage = centeredCollectionViewFlowLayout.currentCenteredPage
        if currentCenteredPage != indexPath.row {
            centeredCollectionViewFlowLayout.scrollToPage(index: indexPath.row, animated: true)
        }
    }
    
}

/// MARK: UITextFieldDelegate
extension MainViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchResultViewTopConstraint.constant = 16
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (Bool) in
            self.searchTextField.becomeFirstResponder()
        }
        DataManager.shared.getLocations(completion: {(_) in
            self.tableView.reloadData()
        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchResultViewTopConstraint.constant = 900
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (Bool) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        DataManager.shared.searchLocation(searchValue: string)
        self.tableView.reloadData()
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            self.heightOfTableViewConstraint.constant = heightOfTableView
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        DataManager.shared.clearSearchString()
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
}

///MARK: UITableViewDelegate
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.shared.getCountOfLocations()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell", for: indexPath) as? LocationTableViewCell
        let location = DataManager.shared.getLocationByIndex(index: indexPath.row)
        cell?.setupCell(city: location.city, country: location.country)
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let location = DataManager.shared.getLocationByIndex(index: indexPath.row)
        DataManager.shared.getForecast(type: "daily", range: "5Day", key: location.key, completion: { (forecast) in
            self.collectionView.reloadData()
            self.searchResultViewTopConstraint.constant = 900
            self.searchTextField.resignFirstResponder()
            self.setupLineChart(forecast: forecast)
        })
        
    }
    func setupLineChart(forecast: Forecast) {
//        let chartConfig = ChartConfigXY(
//            xAxisConfig: ChartAxisConfig(from: 2, to: 14, by: 2),
//            yAxisConfig: ChartAxisConfig(from: 0, to: 14, by: 2)
//        )
//
//        let frame = CGRect(x: 0, y: 70, width: 300, height: 500)
//
//        let chart = LineChart(
//            frame: frame,
//            chartConfig: chartConfig,
//            xTitle: "X axis",
//            yTitle: "Y axis",
//            lines: [
//                (chartPoints: [(2.0, 10.6), (4.2, 5.1), (7.3, 3.0), (8.1, 5.5), (14.0, 8.0)], color: UIColor.red),
//                (chartPoints: [(2.0, 2.6), (4.2, 4.1), (7.3, 1.0), (8.1, 11.5), (14.0, 3.0)], color: UIColor.blue)
//            ]
//        )
//
//        self.view.addSubview(chart.view)
    }
}


